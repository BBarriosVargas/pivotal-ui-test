package com.fundacionjala.pivotal.driver;

import org.openqa.selenium.WebDriver;

public interface IDriver {

    WebDriver initDriver();
}
